FROM python:3.8.2-slim
ENV PYTHONUNBUFFERED 1
ENV VIRTUAL_ENV=/opt/venv

RUN apt-get update && apt-get install gcc gettext -y && apt-get clean
RUN pip install virtualenv && python -m virtualenv $VIRTUAL_ENV

ENV PATH="$VIRTUAL_ENV/bin:$PATH"
COPY ./config/uwsgi /srv/config/uwsgi
COPY ./config/supervisor /srv/config/supervisor
COPY entrypoint.sh /srv/entrypoint.sh

ADD ./src/requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt uwsgi supervisor

COPY ./src /srv/app
WORKDIR /srv/app

ENTRYPOINT ["/srv/entrypoint.sh"]