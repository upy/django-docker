#!/bin/bash
if [ "$SUPERVISOR_CONF_NAME" = "celery_beat" ]; then
	python manage.py migrate
	python manage.py collectstatic --noinput
	python manage.py compilemessages
	python manage.py createsuperuser --username=$DJANGO_SUPER_USERNAME --email=$DJANGO_SUPER_USER_EMAIL --no-input
fi
supervisord -c /srv/config/supervisor/$SUPERVISOR_CONF_NAME.conf